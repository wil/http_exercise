package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/hello", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintln(writer, "hello")
	})
	http.HandleFunc("/hi", func(writer http.ResponseWriter, request *http.Request) {
		key := "put key here"
		value := "put value here"
		statusCode := 12345
		writer.Header().Add(key, value)
		writer.WriteHeader(statusCode)
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
