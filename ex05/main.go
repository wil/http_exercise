package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		f, err := os.Open("index.html")
		if err != nil {
			writer.WriteHeader(404)
			fmt.Fprintf(writer, "cannot load file: %s", err)
			return
		}
		defer f.Close()
		writer.Header().Add("Content-Type", "text/html")
		_, err = io.Copy(writer, f)
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
