package main

import (
	"fmt"
	"log"
	"net"
	"time"
)

var text = []string{
	"A long time ago in a galaxy far, far away...",
	"",
	"A NEW HOPE",
	"",
	"It is a period of civil war.",
	"Rebel spaceships, striking",
	"from a hidden base, have won",
	"their first victory against",
	"the evil Galactic Empire.",
	"",
	"During the battle, Rebel",
	"spies managed to steal secret",
	"plans to the Empire's",
	"ultimate weapon, the DEATH",
	"STAR, an armored space",
	"station with enough power to",
	"destroy an entire planet.",
	"",
	"Pursued by the Empire's",
	"sinister agents, Princess",
	"Leia races home aboard her",
	"starship, custodian of the",
	"stolen plans that can save",
	"her people and restore",
	"freedom to the galaxy...",
}

func handleConnection(conn net.Conn) {
	log.Println("New connection from ", conn.RemoteAddr().String(), "data:")
	defer conn.Close()
	time.Sleep(1 * time.Second)
	for _, l := range text {
		fmt.Fprint(conn, l)
		fmt.Fprint(conn, "\r\n")
		time.Sleep(700 * time.Millisecond)
	}
}

func main() {
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal("[ERROR] cannot open server:", err)
	}
	defer ln.Close()

	log.Println("Listening from ", ln.Addr().String())
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatal("[ERROR] cannot accept new connection:", err)
		}
		go handleConnection(conn)
	}
}
