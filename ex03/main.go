package main

import (
	"io"
	"log"
	"net"
	"os"
)


func handleConnection(conn net.Conn) {
	log.Println("New connection from ", conn.RemoteAddr().String(), "data:")
	io.Copy(os.Stdout, conn)
	conn.Close()
}

func main() {
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal("[ERROR] cannot open server:", err)
	}
	defer ln.Close()

	log.Println("Listening from ", ln.Addr().String())
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatal("[ERROR] cannot accept new connection:", err)
		}
		go handleConnection(conn)
	}
}
