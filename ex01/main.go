package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)


func handleConnection(conn net.Conn) {
	log.Println("New connection from ", conn.RemoteAddr().String())
	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		fmt.Fprintln(conn, scanner.Text())
	}
	conn.Close()
}

func main() {
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal("[ERROR] cannot open server:", err)
	}
	defer ln.Close()

	log.Println("Listening from ", ln.Addr().String())
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Fatal("[ERROR] cannot accept new connection:", err)
		}
		go handleConnection(conn)
	}
}
